package com.example.api_google;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;

import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.util.Log;
import android.widget.ListView;
import android.app.PendingIntent;
import android.preference.PreferenceManager;
import android.content.SharedPreferences;
import android.view.View;
import android.widget.TextView;

import com.ale.infra.contact.RainbowPresence;
import com.ale.infra.http.adapter.concurrent.RainbowServiceException;
import com.ale.infra.manager.room.Room;
import com.ale.infra.proxy.conversation.IRainbowConversation;
import com.ale.infra.proxy.room.IRoomProxy;
import com.ale.infra.proxy.users.IUserProxy;
import com.ale.rainbowsdk.RainbowSdk;
import com.ale.listener.SigninResponseListener;

import com.google.android.gms.location.ActivityRecognitionClient;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.ArrayList;
import android.media.AudioManager;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static java.lang.System.err;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener, SensorEventListener {
    private Context mContext;
    public static final String DETECTED_ACTIVITY = ".DETECTED_ACTIVITY";
    public CountDownTimer ActivityTimer;
    public int CurrentActivity;
    public int PreviousActivity;
//Define an ActivityRecognitionClient//

    public static class Datapoint {
        public float x, y, z;

        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Datapoint)) return false;
            Datapoint v = (Datapoint) o;
            return x == v.x && y == v.y && z == v.z;
        }

        public int hashCode() {
            return 103030;
        }
    }

    // Fall-detection
    private static final int SAMPLING_PERIOD = 10; // Refresh values 10 times per second
    private static final int ALGO_RATE = 500; // Number of millisecond to wait before retriggering algorithm
    private static final int BUFFER_TIMEOUT = 5; // Age of the buffer in seconds
    private static boolean CAN_SEND_RAINBOW = true;
    private static final int MIN_FALL = 40;
    private final Lock mutex = new ReentrantLock(true);
    SensorManager sensorManager;
    Sensor accelerometer;
    LinkedList<Datapoint> points = new LinkedList<>();
    Timer fallDetectionTimer = new Timer();
    TextView fall; // Display on UI fall-detection status


    private ActivityRecognitionClient mActivityRecognitionClient;
    private ActivitiesAdapter mAdapter;

    public List<Room> bubbleList = null;
    public Room eventRoom = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        CurrentActivity=-100;
        PreviousActivity=-100;


        long maxCounter = 99999999;
        long diff = 1000;
        setContentView(R.layout.activity_main);
        TextView timer = (TextView) findViewById(R.id.activities_timer);
        ActivityTimer = new CountDownTimer(maxCounter , diff ) {

            public void onTick(long millisUntilFinished) {
                long diff = maxCounter - millisUntilFinished;
                timer.setText("Temps d'activité : " +diff  / 1000);
                Log.i("Timer","seconds completed: " +diff  / 1000);
                Log.i("test",mAdapter.ProbableActivity.toString());
                Log.i("test Current",Integer.toString(CurrentActivity));
                Log.i("test Previous",Integer.toString(PreviousActivity));
                if (diff==0 || CurrentActivity==-100){
                    CurrentActivity=mAdapter.ProbableActivity.getType();
                    PreviousActivity=CurrentActivity;
                }
                else{
                    PreviousActivity=CurrentActivity;
                    CurrentActivity=mAdapter.ProbableActivity.getType();
                }
                if (CurrentActivity!=PreviousActivity){
                    PreviousActivity=CurrentActivity;
                    this.cancel();
                    this.start();
                }
                else{
                    if(diff/1000>=20){
                        if(CurrentActivity==2 && mAdapter.Running.getConfidence()>mAdapter.Walking.getConfidence()){
                            Log.i("ACTIVITY ACTION", "RUNNING FOR A CERTAIN TIME");
                            rainbowStatusBusy();
                            this.cancel();
                            try {
                                Intent intent = Intent.makeMainSelectorActivity(Intent.ACTION_MAIN, Intent.CATEGORY_APP_MUSIC);
                                startActivity(intent);
                            } catch (Exception e) {
                                Log.d("MUSIC APP LAUNCH", "Exception for launching music player "+e);
                            }
                        }
                        else if(CurrentActivity==2 && mAdapter.Running.getConfidence()<mAdapter.Walking.getConfidence()) {
                            Log.i("ACTIVITY ACTION", "WALKING FOR A CERTAIN TIME");
                            this.cancel();
                            try {
                                am.setRingerMode(1);
                            } catch (Exception e){
                                Log.d("MUTE PHONE", "Exception for muting phone "+e);
                            }

                        }
                        else if(CurrentActivity==3){
                            Log.i("ACTIVITY ACTION", "STANDING STILL");
                            rainbowStatusOnline();
                            this.cancel();
                            this.start();
                        }
                    }
                }
            }

            public void onFinish() {
                timer.setText("done!");
            }

        };

        RainbowSdk.instance().initialize(this, "b162e2e0407211ec9f8f3ba1ac86ab1c", "KTST0L1co7j2VCbmJIoq92iyU5FwRGf8I65GVzx1fpOjtnt0NLGVFY4Jko1leQ3X");

        RainbowSdk.instance().connection().signin("nicolas.eisenberg@al-enterprise.com", "448*m8<zDl56", "sandbox.openrainbow.com", new SigninResponseListener() {
            @Override
            public void onSigninSucceeded() {
                // You are now connected to the sandbox environment
                // Do something on the thread UI
                bubbleList = RainbowSdk.instance().bubbles().findBubblesByName("Fall Bubble");
                if (bubbleList.size() == 0) {
                    RainbowSdk.instance().bubbles().createBubble("Fall Bubble", "Fall detection", true, true, false, new IRoomProxy.IRoomCreationListener() {
                        @Override
                        public void onCreationSuccess(Room room) {
                            eventRoom = room;
                        }

                        @Override
                        public void onCreationFailed(IRoomProxy.RoomCreationError error) {

                        }
                    });
                }
                else {
                    eventRoom = bubbleList.get(0);
                }
            }

            @Override
            public void onRequestFailed(RainbowSdk.ErrorCode errorCode, String err) {
                Log.d("Rainbow", "failed to connec tot rainbow");
                // Do something on the thread UI
            }
        });


        // Load accelerometer
        loadAccel();

        // Start detection of falls at 2Hz
        fallDetectionTimer.schedule(new TimerTask() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void run() {
                fallAlgorithm();
            }
        }, 0, ALGO_RATE);

        fall = (TextView) findViewById(R.id.fall);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACTIVITY_RECOGNITION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACTIVITY_RECOGNITION},
                    100);

        }

        mContext = this;

        //Retrieve the ListView where we’ll display our activity data//
        ListView detectedActivitiesListView = (ListView) findViewById(R.id.activities_listview);

        ArrayList<DetectedActivity> detectedActivities = ActivityIntentService.detectedActivitiesFromJson(
                PreferenceManager.getDefaultSharedPreferences(this).getString(
                        DETECTED_ACTIVITY, ""));

        //Bind the adapter to the ListView//
        mAdapter = new ActivitiesAdapter(this, detectedActivities);
        detectedActivitiesListView.setAdapter(mAdapter);
        mActivityRecognitionClient = new ActivityRecognitionClient(this);


    }

    @Override
    protected void onResume() {
        super.onResume();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);
        updateDetectedActivitiesList();
    }

    @Override
    protected void onPause() {
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    public void requestUpdatesHandler(View view) {
        //restart timer
        ActivityTimer.cancel();
        ActivityTimer.start();
        //reset activities
        CurrentActivity=mAdapter.ProbableActivity.getType();
        PreviousActivity=CurrentActivity;
//Set the activity detection interval.//
        Task<Void> task = mActivityRecognitionClient.requestActivityUpdates(
                1000,
                getActivityDetectionPendingIntent());
        task.addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void result) {
                updateDetectedActivitiesList();
            }
        });
    }

    //Get a PendingIntent//
    private PendingIntent getActivityDetectionPendingIntent() {
//Send the activity data to our DetectedActivitiesIntentService class//
        Intent intent = new Intent(this, ActivityIntentService.class);
        return PendingIntent.getService(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

    }

    //Process the list of activities//
    protected void updateDetectedActivitiesList() {
        ArrayList<DetectedActivity> detectedActivities = ActivityIntentService.detectedActivitiesFromJson(
                PreferenceManager.getDefaultSharedPreferences(mContext)
                        .getString(DETECTED_ACTIVITY, ""));

        mAdapter.updateActivities(detectedActivities);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        if (s.equals(DETECTED_ACTIVITY)) {
            updateDetectedActivitiesList();
        }
    }

    public void loadAccel() {
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(MainActivity.this, accelerometer, SAMPLING_PERIOD, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public boolean detectFreeFall(float[] fallData) {
        int i = 0;
        for (float f : fallData) {
            if (f < 2.34) {
                i++;
            }
        }

        if(i > MIN_FALL) {
            return true;
        }

        return false;
    }

    public boolean detectImpact(float[] fallData) {
        for (float f : fallData) {
            if (f > 102.3) {
                Log.d("FALL DATA", "Detected fall with " + String.valueOf(f) + "ms-2");
                return true;
            }
        }
        return false;
    }

    public void rainbowStatusBusy() {
        RainbowSdk.instance().myProfile().setPresenceTo(RainbowPresence.BUSY, new IUserProxy.IUsersListener(){
            @Override
            public void onSuccess() {
                Log.i("RainbowStatus", "Busy");
            }

            @Override
            public void onFailure(RainbowServiceException exception) {
                Log.e("RainbowStatus", "Busy");
            }
        });
    }

    public void rainbowStatusOnline() {
        RainbowSdk.instance().myProfile().setPresenceTo(RainbowPresence.ONLINE, new IUserProxy.IUsersListener(){
            @Override
            public void onSuccess() {
                Log.i("RainbowStatus", "Online");
            }

            @Override
            public void onFailure(RainbowServiceException exception) {
                Log.e("RainbowStatus", "Online");
            }
        });
    }

    public void rainbowSend(String msg) {
        if(!CAN_SEND_RAINBOW)
            return;

        Log.d("Rainbow send", "Sending event to rainbow");
        IRainbowConversation conversation = RainbowSdk.instance().conversations().getConversationFromRoom(eventRoom);
        RainbowSdk.instance().im().sendMessageToConversation(conversation, msg);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void fallAlgorithm() {
        mutex.lock();
        Log.i("SIZE OF BUFFER", String.valueOf(points.size()));
        if (points.size() < BUFFER_TIMEOUT * SAMPLING_PERIOD) { // We don't have an entire dataset yet
            fall.setText("Disabled");
            points.clear();
            points = new LinkedList<>();
            mutex.unlock();
            return;
        }

        float[] fallData = new float[BUFFER_TIMEOUT * SAMPLING_PERIOD];

        int i = 0;
        for (Datapoint dp : points) {
            fallData[i] = (float) Math.sqrt(dp.x * dp.x + dp.y * dp.y + dp.z * dp.z);
            i++;
        }

        boolean freeFall = detectFreeFall(fallData);
        boolean impacted = detectImpact(fallData);
        if(freeFall || impacted) { // If impacted or freeFall, clear the buffer of values
            points.clear();

            Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));

            rainbowSend(String.format("The phone experienced an event: fall(%b) - impact(%b)", freeFall, impacted));
            CAN_SEND_RAINBOW = true;
        }

        Log.d("Free-fall", "Falling: " + freeFall);
        Log.d("Impact", "Impacted: " + impacted);
        mutex.unlock();
        fall.setText("Falling - Impacted: " + freeFall + " - " + impacted);
        fall.setText("Falling - Impacted: " + detectFreeFall(fallData) + " - " + detectImpact(fallData));


        // End of Rainbow Messaging POC //
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // Add datapoint to the queue
        Datapoint dp = new Datapoint();
        dp.x = sensorEvent.values[0];
        dp.y = sensorEvent.values[1];
        dp.z = sensorEvent.values[2];

        // Add value to queue and remove oldest value if necessary
        mutex.lock();
        points.add(dp);
        if (points.size() > BUFFER_TIMEOUT * SAMPLING_PERIOD) {
            points.remove(0);
        }
        mutex.unlock();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        // Unused
    }
}